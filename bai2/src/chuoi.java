import java.util.Scanner;

public class chuoi {
    public static void main(String[] args) {
        //13. Nhập và từ bàn phím 1 chuỗi và 1 kí tự bất kì. Kiểm tra kí tự vừa nhập vào có thuộc chuỗi hay không?
        // Và kí tự đó xuất hiện mấy lần trong chuỗi
        Scanner sc = new Scanner(System.in);
        System.out.print("nhap chuoi 1 : ");
        String chuoi1 = sc.nextLine();
        System.out.print("nhap chuoi 2 : ");
        String chuoi2 = sc.nextLine();
        System.out.print("nhap ki tu: ");
        char k=sc.next().charAt(0);
        int c=0;
        for (int i = 0; i < chuoi1.length(); i++) {
            if (k == chuoi1.charAt(i)) {
                c++;
            }
        }
        System.out.println("Số lần xuất hiện của ký tự " + k +
                " trong chuỗi " + chuoi1 + " = " + c);

        //14. Nhập vào từ bàn phím 2 chuỗi. Tiến hành nối 2 chuỗi trên thành 1 chuỗi mới

        String S = chuoi1 + chuoi2;
        System.out.print("chuoi moi : " + S);


    }
}
