import java.util.Scanner;

public class mang2chieu {
    public static boolean  ktSNT(int j){
        if (j<2)
            return false;
        for(int i=2; i<j; i++){
            if(j%i == 0){
                return false;
            }
        }
        return true;
    }

    public static void sapXep(int A[][], int m, int n)
    {


        for(int k = 0; k<m ; k++)
            for(int i = 0; i<n-1 ; i++)
                for(int j = 1; j<n ; j++)
                    if(A[k][j]<A[k][j-1])
                    {
                        int temp = A[k][j];
                        A[k][j] = A[k][j-1];
                        A[k][j-1] = temp;
                    }
        for(int i = 0; i<n ; i++)
            for(int k = 0; k<m-1 ; k++)
                for(int j = 1; j<m ; j++)
                    if(A[j][i]<A[j-1][i])
                    {
                        int temp = A[j][i];
                        A[j][i] = A[j-1][i];
                        A[j-1][i] = temp;
                    }
    }
    public static void xuatMang(int B[][], int m, int n){
        System.out.println("xuat mang:");
        for (int i=0;i<3;i++) {
            for (int j = 0; j < 3; j++)
                System.out.print(B[i][j] + "\t");
            System.out.println("\n");
        }

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
       //bai 9: Nhập mảng 2 chiều các số nguyên có dạng ma trận 3x4. Liệt kê các phần tử là số nguyên tố trong có trong mảng.
        System.out.println("nhap mang A:");
        int A[][] = new int[3][4];
        for (int i=0;i<3;i++)
            for (int j=0;j<4;j++)
                A[i][j]=sc.nextInt();

        System.out.print("SNT la: ");
        for (int i=0;i<3;i++) {
            for (int j = 0; j < 4; j++)
                if (ktSNT(A[i][j]))
                    System.out.print(A[i][j]+" ");
        }

        //bai 10:Nhập mảng 2 chiều các số nguyên dương có dạng là ma trận 3x3. Sắp xếp lại các phần tử trong mảng theo thứ tự tăng dần
        System.out.println();
        System.out.println("nhap mang B:");
        int B[][] = new int[3][3];
        for (int i=0;i<3;i++)
            for (int j=0;j<3;j++)
                B[i][j]=sc.nextInt();
        sapXep(B,3,3);
        xuatMang(B,3,3);


    }
}

