package bai1;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main01 {
    public static void main(String[] args) {
        Map map = new HashMap();
        // thêm từ
        map.put("a","1");
        map.put("able","có thể");
        map.put("about","khoảng");
        Set set = map.keySet();
        for (Object key : set) {
            System.out.println(key + " "+ map.get(key));
        }
        // sửa nghĩa
        map.put("a","một");
        for (Object key : set) {
            System.out.println(key + " " + map.get(key));
        }
        //tìm kiếm nghĩa 1 từ
        if (map.containsKey("b") == true){
            System.out.println(map.get("b"));
        }
        else System.out.println("ko tìm thấy");

        //xóa 1 từ
        map.remove("about");
        for (Object key : set) {
            System.out.println(key + " "+ map.get(key));
         }
    }
}
