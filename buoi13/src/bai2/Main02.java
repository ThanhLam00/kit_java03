package bai2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main02 {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<String, String>();
        map.put("12","1100, 16, 12, C");
        map.put("7","0111, 7, 7, 7");
        map.put("1","0001, 1, 1, 1");
        Set set = map.keySet();
        for (Object key : set){
            System.out.println(key + " "+ map.get(key));
        }

        //tìm kiếm + thêm mới
        Scanner sc = new Scanner(System.in);
        System.out.println("nhập key:");
        String a = sc.nextLine();
        if (map.containsKey(a) == true){
            System.out.println(a + " "+ map.get(a));
        }
        else {
            System.out.println("thêm mới: ");
            String b = sc.nextLine();
            map.put(a,b);
        }
        // tìm và xóa
        System.out.println("nhập key:");
        String c = sc.nextLine();
        if (map.containsKey(c) == true){
            map.remove(c);
            for (Object key : set){
                System.out.println(key+ " "+ map.get(key));
            }
        }
        else System.out.println("ko tìm thấy");

    }
}
