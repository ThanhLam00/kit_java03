package bai2;

import java.util.Scanner;

public class Tailieu {
    private String matailieu;
    private String tenTL;
    private String tenNXB;

    public Tailieu(){
        this.matailieu = matailieu;
        this.tenTL = tenTL;
        this.tenNXB = tenNXB;
    }

    public String getMatailieu() {
        return matailieu;
    }

    public void setMatailieu(String matailieu) {
        this.matailieu = matailieu;
    }

    public String getTenTL() {
        return tenTL;
    }

    public void setTenTL(String tenTL) {
        this.tenTL = tenTL;
    }

    public String getTenNXB() {
        return tenNXB;
    }

    public void setTenNXB(String tenNXB) {
        this.tenNXB = tenNXB;
    }

    public void nhapthongtinTL(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ma tai lieu: ");
        this.matailieu = sc.nextLine();
        System.out.println("Ten tai lieu: ");
        this.tenTL= sc.nextLine();
        System.out.println("Ten nha xuat ban: ");
        this.tenNXB = sc.nextLine();
    }

    public void inthongtinTL(){
        System.out.println("Ma tai lieu: "+this.matailieu );
        System.out.println("Ten tai lieu: "+this.tenTL);
        System.out.println("Ten nha xuat ban: "+this.tenNXB);
    }

    public void cungNXB(){
        Sach sach = new Sach();
        Tapchi tapchi = new Tapchi();
        if (sach.getTenNXB()==tapchi.getTenNXB())
            System.out.println("Ten nha xuat ban sach va tap chi la:");
        else System.out.println("khong cung NXB");
    }

    public void incungnam(){
        Sach sach = new Sach();
        Tapchi tapchi = new Tapchi();
        if (sach.getNamXB()==tapchi.getNamXB())
            System.out.println("in cung nam xuat ban");
        else System.out.println("ko in cung nam xuat ban");
        tapchi.getNamXB();
    }
}
