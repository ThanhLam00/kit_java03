package bai2;

import java.util.Scanner;

public class Sach extends Tailieu{
    private int sotrang;
    private int namXB;
    private int lantaiban;
    private String tentacgia;

    public int getSotrang() {
        return sotrang;
    }

    public void setSotrang(int sotrang) {
        this.sotrang = sotrang;
    }

    public int getNamXB() {
        return namXB;
    }

    public void setNamXB(int namXB) {
        this.namXB = namXB;
    }

    public int getLantaiban() {
        return lantaiban;
    }

    public void setLantaiban(int lantaiban) {
        this.lantaiban = lantaiban;
    }

    public String getTentacgia() {
        return tentacgia;
    }

    public void setTentacgia(String tentacgia) {
        this.tentacgia = tentacgia;
    }

    public void nhapthongtinSach(){
        System.out.println("NHAP THONG TIN SACH");
        super.nhapthongtinTL();
        Scanner sc = new Scanner(System.in);
        System.out.println("Ten tac gia: ");
        this.tentacgia = sc.nextLine();
        System.out.println("So trang: ");
        this.sotrang= sc.nextInt();
        System.out.println("Nam xuat ban: ");
        this.namXB = sc.nextInt();
        System.out.println("Lan tai ban: ");
        this.lantaiban = sc.nextInt();
    }

    public void inthongtinSach(){
        System.out.println("IN THONG TIN SACH");
        super.inthongtinTL();
        System.out.println("Ten tac gia: "+this.tentacgia );
        System.out.println("So trang: "+this.sotrang);
        System.out.println("Nam xuat ban: "+this.namXB );
        System.out.println("Lan tai ban: "+this.lantaiban);
    }

}
