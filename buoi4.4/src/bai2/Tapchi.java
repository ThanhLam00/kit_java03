package bai2;

import java.util.Scanner;

public class Tapchi extends Tailieu {
    private int ngayXB,thangXB,namXB;
    private int sotrang;
    private String dangmucXB;

    public int getNgayXB() {
        return ngayXB;
    }

    public void setNgayXB(int ngayXB) {
        this.ngayXB = ngayXB;
    }

    public int getThangXB() {
        return thangXB;
    }

    public void setThangXB(int thangXB) {
        this.thangXB = thangXB;
    }

    public int getNamXB() {
        return namXB;
    }

    public void setNamXB(int namXB) {
        this.namXB = namXB;
    }

    public int getSotrang() {
        return sotrang;
    }

    public void setSotrang(int sotrang) {
        this.sotrang = sotrang;
    }

    public String getDangmucXB() {
        return dangmucXB;
    }

    public void setDangmucXB(String dangmucXB) {
        this.dangmucXB = dangmucXB;
    }

    public void nhapthongtinTC(){
        System.out.println("NHAP THONG TIN TAP CHI");
        super.nhapthongtinTL();
        Scanner sc = new Scanner(System.in);
        System.out.println("Danh muc xuat ban: ");
        this.dangmucXB = sc.nextLine();
        System.out.println("So trang: ");
        this.sotrang= sc.nextInt();
        System.out.println("Ngay xuat ban: ");
        this.ngayXB = sc.nextInt();
        System.out.println("Thang xuat ban: ");
        this.thangXB = sc.nextInt();
        System.out.println("Nam xuat ban: ");
        this.namXB = sc.nextInt();
    }

    public void inthongtinTC(){
        System.out.println("IN THONG TIN TAP CHI");
        super.inthongtinTL();
        System.out.println("Danh muc xuat ban: "+this.dangmucXB );
        System.out.println("So trang: "+this.sotrang);
        System.out.println("Ngay xuat ban: "+this.ngayXB +"/"+this.thangXB+"/"+this.namXB);
    }
}
