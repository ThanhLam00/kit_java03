package bai1;

import java.util.Scanner;

public class Sinhvien extends Human {
    private String malop;
    private String nganhhoc;
    private String khoa;

    public String getMalop() {
        return malop;
    }

    public void setMalop(String malop) {
        this.malop = malop;
    }

    public String getNganhhoc() {
        return nganhhoc;
    }

    public void setNganhhoc(String nganhhoc) {
        this.nganhhoc = nganhhoc;
    }

    public String getKhoa() {
        return khoa;
    }

    public void setKhoa(String khoa) {
        this.khoa = khoa;
    }

    public void nhaphongtinHS(){
        System.out.println("NHAP THONG TIN HOC SINH");
        super.nhapthongtin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Ma lop: ");
        this.malop = sc.nextLine();
        System.out.println("Nganh hoc: ");
        this.nganhhoc = sc.nextLine();
        System.out.println("Khoa: ");
        this.khoa = sc.nextLine();
    }

    public void inhongtinHS(){
        System.out.println("IN THONG TIN HOC SINH");
        super.inthongtin();
        System.out.println("Ma lop: "+this.malop );
        System.out.println("Nganh hoc: "+this.nganhhoc);
        System.out.println("Khoa: "+this.khoa );
    }
    public void gioitinhNu(){
        Sinhvien sinhvien = new Sinhvien();
        if (sinhvien.getGioitinh()=="nu")
            sinhvien.inhongtinHS();
    }
}
