package bai1;

import java.util.Scanner;

public class Human {
    private String hoten;
    private int namsinh;
    private String quequan;
    private String gioitinh;

    public Human(){
        this.hoten = hoten;
        this.namsinh = namsinh;
        this.quequan = quequan;
        this.gioitinh = gioitinh;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public int getNamsinh() {
        return namsinh;
    }

    public void setNamsinh(int namsinh) {
        this.namsinh = namsinh;
    }

    public String getQuequan() {
        return quequan;
    }

    public void setQuequan(String quequan) {
        this.quequan = quequan;
    }

    public String getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(String gioitinh) {
        this.gioitinh = gioitinh;
    }

    public void nhapthongtin(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ho ten: ");
        this.hoten = sc.nextLine();
        System.out.println("Que quan: ");
        this.quequan = sc.nextLine();
        System.out.println("Gioi tinh: ");
        this.gioitinh = sc.nextLine();
        System.out.println("Nam sinh: ");
        this.namsinh = sc.nextInt();
    }

    public void inthongtin(){

        System.out.println("Ho ten: "+ this.hoten);
        System.out.println("Nam sinh: " + this.namsinh );
        System.out.println("Que quan: "+ this.quequan );
        System.out.println("Gioi tinh: "+ this.gioitinh );
    }

    public void cohoihoc(){
        Sinhvien sinhvien = new Sinhvien();
        Giangvien giangvien = new Giangvien();
        if (sinhvien.getNganhhoc()==giangvien.getKhoa())
            System.out.println("sinh vien co co hoi hoc giang vien");
        else System.out.println("sinh vien khong co co hoi hoc giang vien");
    }

}
