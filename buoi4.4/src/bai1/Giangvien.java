package bai1;

import java.util.Scanner;

public class Giangvien extends Human{
    private String khoa;
    private String monhoc;
    private int namKN;

    public String getKhoa() {
        return khoa;
    }

    public void setKhoa(String khoa) {
        this.khoa = khoa;
    }

    public String getMonhoc() {
        return monhoc;
    }

    public void setMonhoc(String monhoc) {
        this.monhoc = monhoc;
    }

    public int getNamKN() {
        return namKN;
    }

    public void setNamKN(int namKN) {
        this.namKN = namKN;
    }

    public void nhaphongtinGV(){
        System.out.println("NHAP THONG TIN GIANG VIEN");
        super.nhapthongtin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Khoa: ");
        this.khoa = sc.nextLine();
        System.out.println("Mon hoc: ");
        this.monhoc = sc.nextLine();
        System.out.println("Nam kinh nghiem: ");
        this.namKN = sc.nextInt();
    }

    public void inhongtinGV(){
        System.out.println("IN THONG TIN GIANG VIEN");
        super.inthongtin();
        System.out.println("Khoa: "+this.khoa );
        System.out.println("Mon hoc: "+this.monhoc);
        System.out.println("Nam kinh nghiem: "+this.namKN );
    }

    public void gioitinhNu(){
        Giangvien giangvien = new Giangvien();
        if (giangvien.getGioitinh()=="nu")
            giangvien.inhongtinGV();
    }
}
