package bai1;

public class Hoadon {
    private String mahoadon;
    private int ngay, thang,nam;
    private String tenkhachhang;
    private String maphong;
    private int dongia;

    public String getMahoadon() {
        return mahoadon;
    }

    public void setMahoadon(String mahoadon) {
        this.mahoadon = mahoadon;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public String getTenkhachhang() {
        return tenkhachhang;
    }

    public void setTenkhachhang(String tenkhachhang) {
        this.tenkhachhang = tenkhachhang;
    }

    public String getMaphong() {
        return maphong;
    }

    public void setMaphong(String maphong) {
        this.maphong = maphong;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public Hoadon(String mahoadon, int ngay, int thang, int nam, String tenkhachhang, String maphong, int dongia){
        this.mahoadon = mahoadon;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.tenkhachhang = tenkhachhang;
        this.maphong = maphong;
        this.dongia = dongia;
    }

    public void inhoadon(){
        System.out.println("Ma hoa don: "+this.mahoadon);
        System.out.println("Ngay hoa don: "+this.ngay+"/"+this.thang+"/"+this.nam);
        System.out.println("Ten khach hang: "+this.tenkhachhang);
        System.out.println("Ma phong: "+this.maphong);
        System.out.println("Don gia: "+this.dongia);
    }





}
