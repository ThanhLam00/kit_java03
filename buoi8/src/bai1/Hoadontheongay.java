package bai1;

public class Hoadontheongay extends Hoadon {
    private int d=0;
    private int songaythue;
    public Hoadontheongay(String mahoadon, int ngay, int thang, int nam, String tenkhachhang, String maphong, int dongia){
        super(mahoadon,ngay,thang,nam,tenkhachhang,maphong,dongia);
        d=d+1;
    }

    public int getSongaythue() {
        return songaythue;
    }

    public void setSongaythue(int songaythue) {
        this.songaythue = songaythue;
    }

    public int thanhtien(){
        if (songaythue<=7)
            return songaythue*getDongia();
        if (songaythue>7)
            return 7*getDongia()+(songaythue-7)*getDongia()*4/5;
        return 0;
    }
    public void inhoadon(){
        super.inhoadon();
        System.out.println("So gio thue: "+this.songaythue);
    }
    public int tongsoluong(){
        return d;
    }
}
