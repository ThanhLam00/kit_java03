package bai1;

public class Hoadontheogio extends Hoadon {
    private int sogiothue;
    private int d ;
    public Hoadontheogio(String mahoadon, int ngay, int thang, int nam, String tenkhachhang, String maphong, int dongia){
        super(mahoadon,ngay,thang,nam,tenkhachhang,maphong,dongia);
    }

    public int getSogiothue() {
        return sogiothue;
    }

    public void setSogiothue(int sogiothue) {
        this.sogiothue = sogiothue;
    }
    public int thanhtien(){
        if (sogiothue<=24)
            return sogiothue*getDongia();
        if (sogiothue>24 && sogiothue<30)
            return 24*getDongia();
        if (sogiothue>30)
            return 0;
        return 0;
    }
    public void inhoadon(){
        super.inhoadon();
        System.out.println("So gio thue: "+this.sogiothue);
    }

    public int tongsoluong(){
        if (thanhtien()!=0)
            return d=d+1;
        return d;
    }
}
