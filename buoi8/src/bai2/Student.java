package bai2;

public class Student extends Person {
    private int diemMH1, diemMH2;
    public Student(String hoten,String diachi){
        super(hoten,diachi);
    }

    public int getDiemMH1() {
        return diemMH1;
    }

    public void setDiemMH1(int diemMH1) {
        this.diemMH1 = diemMH1;
    }

    public int getDiemMH2() {
        return diemMH2;
    }

    public void setDiemMH2(int diemMH2) {
        this.diemMH2 = diemMH2;
    }
    public int diemTB(){
        return (diemMH1+diemMH2)/2;
    }

    public void xuat(){
        super.xuat();
        System.out.println("Diem mon hoc 1: "+this.diemMH1);
        System.out.println("Diem mon hoc 2: "+this.diemMH2);
        System.out.println("Diem trung binh: "+diemTB());
    }
}
