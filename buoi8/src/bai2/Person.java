package bai2;

public class Person {
    private String hoten;
    private String diachi;

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }
    public Person(String hoten,String diachi){
        this.hoten = hoten;
        this.diachi = diachi;
    }
    public void xuat(){
        System.out.println("Ho ten: "+this.hoten);
        System.out.println("Dia chi: "+this.diachi);
    }
}
