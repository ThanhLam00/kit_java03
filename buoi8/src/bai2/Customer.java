package bai2;

public class Customer extends Person {
    private String tencongty;
    private int trigiahoadon;
    private String danhgia;

    public Customer(String hoten, String diachi) {
        super(hoten, diachi);
    }
    public void xuat(){
        super.xuat();
        System.out.println("Ten cong ty: "+this.tencongty);
        System.out.println("Tri gia hoa don: "+this.trigiahoadon);
        System.out.println("Danh gia: "+this.danhgia);
    }
}
