package bai2;

public class Employee extends Person {

    private int heSoLuong;
    private String danhgia;
    public Employee(String hoten, String diachi) {
        super(hoten, diachi);
    }
    public int tinhluong(){
        return heSoLuong*30;
    }
    public void xuat(){
        super.xuat();
        System.out.println("He so luong: "+this.heSoLuong);
        System.out.println("Danh gia: "+this.danhgia);
        System.out.println("Tien luong: "+tinhluong());
    }
}
