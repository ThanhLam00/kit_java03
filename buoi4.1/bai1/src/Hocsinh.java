import java.util.Scanner;

public class Hocsinh {
    private String hoten;
    private int ngay, thang, nam;
    private String quequan;

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public String getQuequan() {
        return quequan;
    }

    public void setQuequan(String quequan) {
        this.quequan = quequan;
    }

    public void nhapthongtin(){
        Scanner sc=new Scanner(System.in);
        System.out.println("nhap ho ten:");
        this.hoten = sc.nextLine();
        System.out.println("nhap que quan:");
        this.quequan = sc.nextLine();
        System.out.println("nhap ngay, thang, nam sinh:");
        this.ngay = sc.nextInt();
        this.thang = sc.nextInt();
        this.nam = sc.nextInt();
    }

    public void inthongtin(){
        System.out.println("ho ten: "+this.hoten);
        System.out.println("ngay, thang, nam sinh: "+ this.ngay+"/"+this.thang+"/"+this.nam);
        System.out.println("que quan: "+this.quequan);
    }

    public void inhoaten(){
        String str = this.hoten.toUpperCase();
        System.out.println("ho ten viet in hoa: "+str);
    }

    public void suathongtin(){
        Hocsinh hocsinh = new Hocsinh();
        Scanner sc= new Scanner(System.in);
        hocsinh.nhapthongtin();
        System.out.println("Sua que quan:");
        hocsinh.quequan = sc.nextLine();
        System.out.println("Sua ngay, thang, nam sinh: ");
        hocsinh.ngay = sc.nextInt();
        hocsinh.thang = sc.nextInt();
        hocsinh.nam = sc.nextInt();
        hocsinh.inthongtin();
    }

    public void songay(){
        System.out.println("so ngay da song: "+(this.ngay+this.thang*30+(2020-this.nam)*365));
    }

}
