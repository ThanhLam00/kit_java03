import java.util.Scanner;

public class Sach {
    private String tensach;
    private String NXB;
    private int sotrang;
    private int giaban;

    public void nhapthongtin(){
        Scanner sc=new Scanner(System.in);
        System.out.println("nhap ten sach:");
        this.tensach = sc.nextLine();
        System.out.println("nhap ten nha xuat ban:");
        this.NXB = sc.nextLine();
        System.out.println("nhap so trang:");
        this.sotrang = sc.nextInt();
        System.out.println("nhap gia ban:");
        this.giaban = sc.nextInt();
    }

    public void inthongtin(){
        System.out.println("ten sach: " +this.tensach);
        System.out.println("ten nha xuat ban: "+this.NXB);
        System.out.println("so trang: "+this.sotrang);
        System.out.println("gia ban: "+this.giaban);
    }

    public void sachgiacaohon(){
       Sach sach1 = new  Sach();
       Sach sach2 = new  Sach();
       System.out.println("nhap thong tin sach 1:");
       sach1.nhapthongtin();
       System.out.println("nhap thong tin sach 2:");
       sach2.nhapthongtin();
       System.out.println("THONG TIN SACH GIA CAO HON:");
       if (sach1.giaban>sach2.giaban)
           sach1.inthongtin();
       else sach2.inthongtin();
    }

    public void thongbaosachgia(){
        Sach sach1 = new  Sach();
        Sach sach2 = new  Sach();
        System.out.println("nhap thong tin sach 1:");
        sach1.nhapthongtin();
        System.out.println("nhap thong tin sach 2:");
        sach2.nhapthongtin();
        if (sach1.tensach==sach2.tensach&&sach1.NXB==sach2.NXB)
            System.out.println("SACH THAT!");
        else System.out.println("SACH GIA!");
    }

}
