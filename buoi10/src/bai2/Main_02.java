package bai2;

public class Main_02 {
    public static void main(String[] args) {
        KhoiA khoiA = new KhoiA("a123","A","ha noi","co",8,7,8);
        khoiA.xuat();
        KhoiB khoiB = new KhoiB("a567","W","hai duong","co",7,7,7);
        khoiB.xuat();
        KhoiC khoiC = new KhoiC("a45","T","ha noi","ko",7,9,8);
        Tuyensinh tuyensinh = new Tuyensinh() {
            @Override
            public void xuat() {

            }
        };
        System.out.println("TIM THEO SO BAO DANH");
        ThiSinh thiSinh[] = {khoiA,khoiB,khoiC};
        for (int i=0;i<thiSinh.length;i++){
            ThiSinh thisinh = thiSinh[i];
            if (thisinh.getSBD().contains("123")){
                thisinh.xuat();
            }
        }

    }
}
