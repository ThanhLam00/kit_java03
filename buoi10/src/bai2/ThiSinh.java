package bai2;

public class ThiSinh extends Tuyensinh {
    private String SBD;
    private String hoten;
    private String diachi;
    private String uutien;

    public String getSBD() {
        return SBD;
    }

    public void setSBD(String SBD) {
        this.SBD = SBD;
    }

    public ThiSinh(String SBD, String hoten, String diachi, String uutien){
        this.SBD = SBD;
        this.hoten = hoten;
        this.diachi = diachi;
        this.uutien = uutien;
    }
     @Override
    public void xuat(){
        System.out.println("So bao danh: " +SBD);
        System.out.println("Ho ten: "+hoten);
        System.out.println("Dia chi: "+diachi);
        System.out.println("Uu tien: "+uutien);
    }
    //public abstract void xuat();
}
