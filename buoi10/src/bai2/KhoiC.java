package bai2;

public class KhoiC extends ThiSinh {
    private double van;
    private double su;
    private double dia;

    public KhoiC(String SBD, String hoten, String diachi, String uutien, double van, double su, double dia) {
        super(SBD, hoten, diachi, uutien);
        this.van = van;
        this.su = su;
        this.dia = dia;
    }

    @Override
    public void xuat() {
        super.xuat();
        System.out.println("Diem van: " + van);
        System.out.println("Diem su: " + su);
        System.out.println("Diem dia: " + dia);
    }
}
