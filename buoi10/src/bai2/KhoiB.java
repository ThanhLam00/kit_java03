package bai2;

public class KhoiB extends ThiSinh {
    private double toan;
    private double sinh;
    private double hoa;

    public KhoiB(String SBD, String hoten, String diachi, String uutien, double toan, double hoa, double sinh) {
        super(SBD, hoten, diachi, uutien);
        this.toan = toan;
        this.hoa = hoa;
        this.sinh = sinh;
    }

    @Override
    public void xuat() {
        super.xuat();
        System.out.println("Diem toan: " + toan);
        System.out.println("Diem hoa: " + hoa);
        System.out.println("Diem sinh: " + sinh);
    }
}
