package bai3;

public class Nguoi extends Khupho {
    private String hoten;
    private int tuoi;
    private String namsinh;
    private String nghenghiep;
    public Nguoi(int sothanhvien, int sonha, String hoten, int tuoi, String namsinh, String nghenghiep) {
        super(sothanhvien, sonha);
        this.hoten =hoten;
        this.tuoi = tuoi;
        this.namsinh = namsinh;
        this.nghenghiep = nghenghiep;
    }
    public void xuat(){
        super.xuat();
        System.out.println("Ho ten: "+hoten);
        System.out.println("Tuoi: "+tuoi);
        System.out.println("Nam sinh: "+namsinh);
        System.out.println("Nghe nghiep: "+nghenghiep);
    }
}
