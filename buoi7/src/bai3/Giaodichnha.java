package bai3;

public class Giaodichnha extends Giaodichdat {
    public Giaodichnha(String magiaodich, int ngay, int thang, int nam,int dongia, int dientich){
        super(magiaodich,ngay,thang,nam,dongia,dientich);
    }
    private String loainha;
    private String diachi;

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getLoainha() {
        return loainha;
    }

    public void setLoainha(String loainha) {
        this.loainha = loainha;
    }
    public int thanhtien(){
        if (loainha == "thuong") {
            return getDientich() * getDongia() * 9 / 10;
        }
        else{
            return getDientich()*getDongia();
        }
    }
    public void xuatGD(){
        System.out.println("Ma giao dich: "+getMagiaodich());
        System.out.println("Ngay nhap: "+getNgay()+"/"+getThang()+"/"+getNam());
        System.out.println("Don gia: "+getDongia());
        System.out.println("Dien tich: "+getDientich());
        System.out.println("Loai nha: "+this.loainha);
        System.out.println("Dia chi: "+this.diachi);
    }
    public int tongSL(int soluong1,int soluong2){
        return soluong1+soluong2;
    }

    public void gdT2_2020(){
        if (getThang()==2 && getNam()==2020)
            xuatGD();
    }
}
