package bai3;

public class Giaodichdat {
    private String magiaodich;
    private int ngay, thang,nam;
    private int dongia;
    private String loaidat;
    private int dientich;

    public String getMagiaodich() {
        return magiaodich;
    }

    public void setMagiaodich(String magiaodich) {
        this.magiaodich = magiaodich;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public String getLoaidat() {
        return loaidat;
    }

    public void setLoaidat(String loaidat) {
        this.loaidat = loaidat;
    }

    public int getDientich() {
        return dientich;
    }

    public void setDientich(int dientich) {
        this.dientich = dientich;
    }

    public int thanhtien(){
        if (loaidat == "B"|| loaidat =="C")
            return dientich*dongia;
        if (loaidat=="A")
            return dientich*dongia*3/2;
        return 0;
    }
    public Giaodichdat(String magiaodich, int ngay, int thang, int nam,int dongia,String loaidat,int dientich){
        this.magiaodich = magiaodich;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.dongia = dongia;
        this.loaidat = loaidat;
        this.dientich = dientich;
    }

    public Giaodichdat(String magiaodich, int ngay, int thang, int nam,int dongia,int dientich){
        this.magiaodich = magiaodich;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.dongia = dongia;
        this.dientich = dientich;
    }
    public void xuatGD(){
        System.out.println("Ma giao dich: "+this.magiaodich);
        System.out.println("Ngay nhap: "+this.ngay+"/"+this.thang+"/"+this.nam);
        System.out.println("Don gia: "+this.dongia);
        System.out.println("Loai dat: "+this.loaidat);
        System.out.println("Dien tich: "+this.dientich);
    }
    public int tongSL(int soluong1,int soluong2){
        return soluong1+soluong2;
    }
    public int tbthanhtien(int thanhtien1, int thanhtien2){
        return (thanhtien1+thanhtien2)/2;
    }
    public void gdT2_2020(){
        if (thang==2 && nam==2020)
            xuatGD();
    }
}
