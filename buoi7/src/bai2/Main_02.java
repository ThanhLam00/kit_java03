package bai2;

public class Main_02 {
    public static void main(String[] args) {
        Giaodichvang giaodichvang = new Giaodichvang("M01",1,1,2000,1000000009,1);
        Giaodichtiente giaodichtiente = new Giaodichtiente("N01",1,3,2000,8,2);
        giaodichvang.setLoaivang("999");
        giaodichtiente.setTigia(2);
        giaodichtiente.setLoaitiente("USD");

        giaodichvang.xuatGD();
        giaodichtiente.xuatGD();

        System.out.println("thanh tien GDV: "+giaodichvang.thanhtien());
        System.out.println("thanh tien GDTT: "+giaodichtiente.thanhtien());

        giaodichvang.tren1ty();
        giaodichtiente.tren1ty();
    }
}
