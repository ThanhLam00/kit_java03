package bai2;

public class Giaodich {
    private String magiaodich;
    private int ngay, thang,nam;
    private int dongia;
    private int soluong;
    public String getMagiaodich() {
        return magiaodich;
    }

    public void setMagiaodich(String magiaodich) {
        this.magiaodich = magiaodich;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public Giaodich(String magiaodich, int ngay, int thang, int nam,int dongia, int soluong){
        this.magiaodich = magiaodich;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.dongia = dongia;
        this.soluong = soluong;
    }
    public void xuatGD(){
        System.out.println("Ma giao dich: "+this.magiaodich);
        System.out.println("Ngay nhap: "+this.ngay+"/"+this.thang+"/"+this.nam);
        System.out.println("Don gia: "+this.dongia);
        System.out.println("So luong: "+this.soluong);
    }

}
