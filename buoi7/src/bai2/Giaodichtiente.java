package bai2;

public class Giaodichtiente extends Giaodich{
    Giaodichtiente(String magiaodich, int ngay, int thang, int nam,int dongia, int soluong){
        super(magiaodich,ngay,thang,nam,dongia,soluong);
    }
    private int tigia;
    private String loaitiente;

    public int getTigia() {
        return tigia;
    }

    public void setTigia(int tigia) {
        this.tigia = tigia;
    }

    public String getLoaitiente() {
        return loaitiente;
    }

    public void setLoaitiente(String loaitiente) {
        this.loaitiente = loaitiente;
    }

    public void xuatGD(){
        super.xuatGD();
        System.out.println("Ti gia: "+ this.tigia);
        System.out.println("Loai tien te: "+this.loaitiente);
    }

    public int thanhtien(){
        if (loaitiente =="USD" || loaitiente == "Euro") {
            return getDongia()*getSoluong()*tigia;
        }
        if (loaitiente == "VN")
            return getDongia()*getSoluong();
        return 0;
    }
    public int tongSL(int soluong1,int soluong2){
        return soluong1+soluong2;
    }
    public int tbthanhtien(int thanhtien1, int thanhtien2) {
        return (thanhtien1 + thanhtien2) / 2;
    }
    public void tren1ty(){
        if (getDongia()>1000000000)
            xuatGD();
    }
}
