package bai2;

public class Giaodichvang extends Giaodich{

    public Giaodichvang(String magiaodich, int ngay, int thang, int nam,int dongia, int soluong){
        super(magiaodich,ngay,thang,nam,dongia,soluong);
    }

    private String loaivang;


    public String getLoaivang() {
        return loaivang;
    }

    public void setLoaivang(String loaivang) {
        this.loaivang = loaivang;
    }

    public int thanhtien(){
        return getSoluong()*getDongia();
    }

    public void xuatGD(){
        super.xuatGD();
        System.out.println("Loai vang: "+this.loaivang);
    }
    public int tongSL(int soluong1,int soluong2){
        return soluong1+soluong2;
    }
    public void tren1ty(){
        if (getDongia()>1000000000)
            xuatGD();
    }

}
