package bai1;

public class Sachgiaokhoa {
    private String masach;
    private int ngay, thang,nam;
    private int dongia;
    private int soluong;
    private String NXB;
    private String tinhtrang;

    public String getMasach() {
        return masach;
    }

    public void setMasach(String masach) {
        this.masach = masach;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public String getNXB() {
        return NXB;
    }

    public void setNXB(String NXB) {
        this.NXB = NXB;
    }

    public String getTinhtrang() {
        return tinhtrang;
    }

    public void setTinhtrang(String tinhtrang) {
        this.tinhtrang = tinhtrang;
    }

    public Sachgiaokhoa(String masach, int ngay, int thang, int nam,int dongia, int soluong, String NXB,String tinhtrang){
        this.masach = masach;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.dongia = dongia;
        this.soluong = soluong;
        this.NXB = NXB;
        this.tinhtrang = tinhtrang;
    }
    public Sachgiaokhoa(String masach, int ngay, int thang, int nam,int dongia, int soluong, String NXB){
        this.masach = masach;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.dongia = dongia;
        this.soluong = soluong;
        this.NXB = NXB;
    }
    public int thanhtien(){
        if (tinhtrang == "moi")
            return soluong*dongia;
        else return (soluong*dongia/2);
    }
    public int tongthanhtien(int thanhtien1, int thanhtien2) {
        return (thanhtien1 + thanhtien2);
    }

    public void xuatDS(){
        System.out.println("Ma sach: "+this.masach);
        System.out.println("Ngay nhap: "+this.ngay);
        System.out.println("Thang nhap: "+this.thang);
        System.out.println("Nam nhap: "+this.nam);
        System.out.println("Don gia: "+dongia);
        System.out.println("So luong: "+this.soluong);
        System.out.println("Nha xuat ban: "+this.NXB);
        System.out.println("Tinh trang: "+this.tinhtrang);

    }


}
