package bai1;

public class Sachthamkhao extends Sachgiaokhoa {
    private int thue;

    public Sachthamkhao(String masach, int ngay, int thang, int nam,int dongia, int soluong, String NXB){
        super(masach,ngay,thang,nam,dongia,soluong,NXB);
    }

    public int getThue() {
        return thue;
    }

    public void setThue(int thue) {
        this.thue = thue;
    }

    public void xuatDS(){
        super.xuatDS();
        System.out.println("Thue: "+this.thue);
    }

    public int thanhtien(){
        return getSoluong()*getDongia()+ thue;
    }
    public int tbthanhtien(int thanhtien1, int thanhtien2) {
        return (thanhtien1 + thanhtien2);
    }
    public int tbdongia(int dongia1, int dongia2) {
        return (dongia1 + dongia2) / 2;
    }

    public int trungbinhcong(){
        return thanhtien()/getSoluong();
    }

}
