public class Nuocngoai extends VietNam {
    private String quoctich;

    public Nuocngoai(String makhachhang, String hoten, String ngayhoadon,String quoctich, int soluong, int dongia) {
        super(makhachhang, hoten, ngayhoadon, soluong, dongia);
        this.quoctich = quoctich;
    }
    public int thanhtien(){
            return getSoluong()*getDongia();
    }
    public void xuat(){
        System.out.println("Ma khach hang: "+getMakhachhang());
        System.out.println("Ho ten: "+ getHoten() );
        System.out.println("Ngay ra hoa don: "+getNgayhoadon());
        System.out.println("Doi tuong: "+this.quoctich);
        System.out.println("So luong: "+getSoluong());
        System.out.println("Don gia: "+getDongia());
        System.out.println("Thanh tien: "+thanhtien());
    }
}
