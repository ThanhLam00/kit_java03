public class VietNam {
    private String makhachhang;
    private String hoten;
    private String ngayhoadon;
    private String doituong;
    private int soluong;
    private int dongia;
    private int dinhmuc;

    public String getMakhachhang() {
        return makhachhang;
    }

    public void setMakhachhang(String makhachhang) {
        this.makhachhang = makhachhang;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public String getNgayhoadon() {
        return ngayhoadon;
    }

    public void setNgayhoadon(String ngayhoadon) {
        this.ngayhoadon = ngayhoadon;
    }

    public String getDoituong() {
        return doituong;
    }

    public void setDoituong(String doituong) {
        this.doituong = doituong;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public int getDinhmuc() {
        return dinhmuc;
    }

    public void setDinhmuc(int dinhmuc) {
        this.dinhmuc = dinhmuc;
    }

    public int thanhtien(){
        if (soluong <= dinhmuc)
            return soluong*dongia;
        else
            return soluong*dongia*dinhmuc+(soluong-dinhmuc)*dongia*5/2;
    }
    public VietNam(String makhachhang,String hoten,String ngayhoadon,String doituong, int soluong ,int dongia,int dinhmuc ){
        this.makhachhang = makhachhang;
        this.hoten = hoten;
        this.ngayhoadon = ngayhoadon;
        this.doituong = doituong;
        this.soluong = soluong;
        this.dongia = dongia;
        this.dinhmuc = dinhmuc;
    }
    public VietNam(String makhachhang,String hoten,String ngayhoadon, int soluong ,int dongia ){
        this.makhachhang = makhachhang;
        this.hoten = hoten;
        this.ngayhoadon = ngayhoadon;
        this.soluong = soluong;
        this.dongia = dongia;
    }
     public void xuat(){
         System.out.println("Ma khach hang: "+this.makhachhang);
         System.out.println("Ho ten: "+ this.hoten);
         System.out.println("Ngay ra hoa don: "+this.ngayhoadon);
         System.out.println("Doi tuong: "+this.doituong);
         System.out.println("So luong: "+this.soluong);
         System.out.println("Don gia: "+this.dongia);
         System.out.println("Dinh muc: "+this.dinhmuc);
         System.out.println("Thanh tien: "+thanhtien());
     }
}
