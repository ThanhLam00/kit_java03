public class Main {
    public static void main(String[] args) {
        VietNam vietNam1 = new VietNam("001","a","01/01/2019","sinh hoat",10,5,2);
        vietNam1.xuat();
        VietNam vietNam2 = new VietNam("011","b","11/09/2019","sinh hoat",12,5,2);
        vietNam2.xuat();

        Nuocngoai nuocngoai1 = new Nuocngoai("002","m","04/01/2020","anh",8,5);
        nuocngoai1.xuat();
        Nuocngoai nuocngoai2 = new Nuocngoai("012","n","05/01/2020","my",15,5);
        nuocngoai2.xuat();

        Nuocngoai[] Arr = {nuocngoai1,nuocngoai2};
        double totalPrice = 0;
        int temp = 0;
        for (int i = 0; i < Arr.length; i++) {
            Nuocngoai arr = Arr[i];
            {
                totalPrice = totalPrice + arr.thanhtien();
                temp++;
            }
        }
        System.out.println("trung bình thành tiền của khach hang nuoc ngoai:");
        System.out.println((totalPrice) / temp);

        System.out.println("Hoa don trong thang 09/2019:");
        VietNam[] Array = {vietNam1,vietNam2,nuocngoai1,nuocngoai2};
        for (int i = 0; i < Array.length; i++) {
            VietNam array = Array[i];
            {
                if (array.getNgayhoadon().contains("/09/2019")) {
                    array.xuat();
                }
            }
        }
    }
}
