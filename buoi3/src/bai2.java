import java.util.Scanner;
/*Bài 2: Nhập vào từ bàn phím ma trận số nguyên m*n (m,n là 2 số nguyên dương nhập vào từ bàn phím). Sử dụng hàm để thực hiện các chức năng sau:
- In ra màn hình các phần tử thuộc đường chéo chính.
- In ra màn hình danh sách số nguyên tố của 1 hàng bất kỳ trong mảng ban đầu
- In ra màn hình danh sách các số chính phương của 1 cột bất kỳ trong mảng ban đầu.
- Tính tổng các số phần tử trên 1 hàng bất kỳ thuộc mảng.
 */

public class bai2 {

    private static void duongCheoChinh(int a[][], int n,int m){
        System.out.println("các phần tử thuộc đường chéo chính: ");
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
                if (i==j)
                    System.out.print(a[i][j]+" ");
    }

    private static boolean laSNT(int t){
        if (t<2)
            return false;
        for (int i=2;i<t;i++) {
            if (t % i == 0)
                return false;
            else
                return true;
        }
        return true;
    }

    public static boolean soChinhPhuong(int n) {

        int temp = (int)Math.sqrt(n);
        if(temp*temp == n) {
            return true;
        }
        else {
            return false;
        }
    }

    private static void Sum(int a[][], int n,int m,int h){
        System.out.println("\nTính tổng các số phần tử trên hàng "+h+" thuộc mảng: ");
        int S=0;
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
                if (i==h)
                    S=S+a[i][j];
        System.out.println("Tong la: "+S);
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("nhap n, m:");
        int n=sc.nextInt();
        int m=sc.nextInt();
        System.out.println("nhap mang:");
        int a[][] = new int[n][m];
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
                a[i][j]= sc.nextInt();
        System.out.println("in mang:");
        for (int i=0;i<n;i++) {
            for (int j = 0; j < m; j++)
                System.out.print(a[i][j] + "\t");
            System.out.println("\n");
        }

        duongCheoChinh(a,n,m);
        System.out.println("\nnhap hang:");
        int h=sc.nextInt();
        System.out.println("số nguyên tố của hàng "+h+" :");
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
                if (i==h){
                    if(laSNT(a[i][j]))
                       System.out.print(a[i][j]+" ");
                }

        System.out.println("\nnhap cot:");
        int c=sc.nextInt();
        System.out.println("số chính phương của cột "+c+" :");
        for (int i=0;i<n;i++)
            for (int j=0;j<m;j++)
                if (j==c){
                    if(soChinhPhuong(a[i][j]))
                        System.out.print(a[i][j]+" ");
                }

        Sum(a,n,m,h);
    }
}
