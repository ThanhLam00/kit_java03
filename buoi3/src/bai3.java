import java.util.Scanner;

/*Bài 3: Nhập vào từ bàn phìm 1 chuỗi bất kỳ. Sử dụng hàm để thực hiện các chức năng sau:
- Viết hoa toàn bộ chuỗi.
- Nhập vào từ bàn phím 1 ký tự bất kỳ. Kiểm tra kí tự đó có thuộc chuỗi ban đầu không. Nếu có thì biến đổi kí tự đó sang mã hex
INPUT: Hello World, k = o;
OUTPUT: true, Hell6F W6Frld.
- Viết hoa các chữ cái đầu của từ.
INPUT: hello world
OUTPUT: Hello World
 */

public class bai3 {

    private static void vietHoaChuoi(String chuoi) {
        String chuoi1 = chuoi.toUpperCase();
        System.out.println("Viết hoa toàn bộ chuỗi: " + chuoi1);
    }

    private static void kiemtrachuoi(String chuoi, String kytu) {
        int d=0;
        for (int i=0;i<chuoi.length();i++){
            if (kytu.charAt(0)==chuoi.charAt(i))
                d++;
        }
        if (d==0)
            System.out.println("kí tự "+kytu+" ko thuộc chuỗi ban đầu");
        else
            System.out.println("kí tự "+kytu+" co thuộc chuỗi ban đầu");
    }

    private static void vietHoaCCD(String chuoi) {
        String[] arr = chuoi.split(" ");//cắt string thành mảng qua các dấu Space
        String chuoi1 = "";
        for (String x : arr) {//dùng vòng lặp duyệt các từ và thay thế từ đầu tiên!
            chuoi1 = chuoi1 + (x.substring(0, 1).toUpperCase() + x.substring(1));
            chuoi1 = chuoi1 + " ";
        }
        System.out.println("Viết hoa các chữ cái đầu của từ: "+chuoi1);
    }

    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("nhap chuoi :");
        String chuoi = sc.nextLine();
        vietHoaChuoi(chuoi);
        System.out.println("nhap ky tu:");
        String kytu =sc.nextLine();
        kiemtrachuoi(chuoi,kytu);
        vietHoaCCD(chuoi);
    }
}
