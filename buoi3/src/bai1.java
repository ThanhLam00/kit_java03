import java.util.Scanner;
/*Bài 1: Nhập vào từ bàn phím mảng số nguyên gồm n phần tử. Sử dụng hàm để thực hiện các chức năng sau:
- In ra màn hình giá trị MAX, MIN trong mảng số nguyên
- In ra màn hình danh sách các số nguyên chẵn.
- In ra màn hình danh sách các số nguyên chia hết cho cả 2 và 3.
- In ra màn hình các số là ước của MAX.
- In ra màn hình các số là bội của MIN */
public class bai1 {

    private static void timMaxMin(int arr[],int n){
        int Max=arr[0];
        int Min=arr[0];
        for (int i=0;i<n;i++){
            if (Max<arr[i])
                Max=arr[i];
            if (Min>arr[i])
                Min=arr[i];
        }
        System.out.println("GTLN la: "+Max);
        System.out.println("GTNN la: "+Min);
    }

    private static void soNguyenChan(int arr[],int n){
        System.out.println("so nguyen chan la: ");
        for (int i=0;i<n;i++){
            if (arr[i]%2==0)
                System.out.print(arr[i]+" ");
        }
    }

    private static void soNguyenChiaHet(int arr[],int n){
        System.out.println("\nso chia het cho 2 va 3 la: ");
        for (int i=0;i<n;i++){
            if (arr[i]%2==0 && arr[i]%3==0)
                System.out.print(arr[i]+" ");
        }
    }

    private static void uocMax(int arr[],int n) {
        int Max = arr[0];
        for (int i = 0; i < n; i++) {
            if (Max < arr[i])
                Max = arr[i];
        }
        System.out.println("\nso la uoc cua Max la:");
        for (int i = 0; i < n; i++) {
            if (Max % arr[i] == 0)
                System.out.print(arr[i] + " ");
        }
    }
    private static void boiMin(int arr[],int n) {
        int Min = arr[0];
        for (int i = 0; i < n; i++) {
            if (Min > arr[i])
                Min = arr[i];
        }
        System.out.println("\nso la boi cua Min la:");
        for (int i = 0; i < n; i++) {
            if (arr[i]%Min == 0)
                System.out.print(arr[i] + " ");
        }
    }
        public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("nhap n: ");
        int n =sc.nextInt();
        System.out.println("nhap mang: ");
        int arr[] = new int[n];
        for (int i=0;i<n;i++)
            arr[i]=sc.nextInt();
        timMaxMin(arr,n);
        soNguyenChan(arr,n);
        soNguyenChiaHet(arr,n);
        uocMax(arr,n);
        boiMin(arr,n);
    }
}
